#!/usr/bin/env python3

import configparser
from datetime import date, datetime, timedelta
from enum import Enum
import pickle
import random
from typing import Any, Dict, List, Optional
import paho.mqtt.client as mqtt                        # see documentation in https://github.com/eclipse/paho.mqtt.python
import json
import time
import numpy as np
from pathlib import Path
import requests

# Diccionario para mapear los números a los nombres en español
dias_semana_numero_nombre = {
    0: "LUNES",
    1: "MARTES",
    2: "MIERCOLES",
    3: "JUEVES",
    4: "VIERNES",
    5: "SABADO",
    6: "DOMINGO"
}

# Diccionario para mapear los números a los nombres en español
dias_semana_nombre_numero = {
    "LUNES"     : 0, 
    "MARTES"    : 1,
    "MIERCOLES" : 2,
    "JUEVES"    : 3,
    "VIERNES"   : 4,
    "SABADO"    : 5,
    "DOMINGO"   : 6
}

bookingStates = {
    1: "RESERVE",
    6: "ENDED",
    7: "COMPLETED"
} 

class LocalBookingStates(Enum):
    NOTRESERVED = "NOTRESERVED"
    CANCELLED = "CANCELLED"
    INLIST = "INLIST"
    RESERVED = "RESERVED"

# Diccionario para mapear los números a los nombres en español
plazas_disponibles_minutos_espera = {
    1: 5,
    2: 5,
    3: 10,
    4: 10,
    5: 30,
    6: 60,
    7: 120,
    8: 120,
    9: 160,
    10: 160,
    11: 160,
    12: 160,
    13: 160,
    14: 160
}

config_ini = "trainingymapp.ini"
config_parser = configparser.ConfigParser()
config_parser.optionxform = str
config_parser.read(config_ini)                         # read config file and store its values in config_parser variable 

mqtt_host = config_parser.get("MQTT", "host") 
mqtt_topic = config_parser.get("MQTT", "topic")

user = config_parser.get("Trainingymapp", "user")
config_ini_private = user + "_private.ini"

config_parser_private = configparser.ConfigParser()
config_parser_private.optionxform = str
config_parser_private.read(config_ini_private)          # read config file and store its values in config_parser variable 

# Headers que simulan los de un navegador legítimo
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.9",
    "Accept-Encoding": "gzip, deflate, br",
    "Referer": "https://example.com/login",
    "Connection": "keep-alive",
    "Content-Type": "application/x-www-form-urlencoded"
}

# Callbacks
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Conectado al servidor MQTT")
    else:
        print(f"Error de conexión: {rc}")

def initMQTT() -> Optional[mqtt.Client]: 
    mqttClient = mqtt.Client()
    try:         
        mqttClient.on_connect = on_connect       
        mqttClient.connect(mqtt_host)
                                                         # Starts a new thread, that calls the loop method at regular intervals for you. It also handles re-connects automatically.
        mqttClient.loop_start()                          # Non blocking method. We want to manage the fan though no MQTT server is present 

        return mqttClient
    except:   
        print("Error inicializando MQTT")                                                  # Error control. The program will continue though there is no MQTT server
        return None

def publish_mqtt(content : str):
    payload = {}
    
    content = "[Gym][" + user + "]:" + content
    payload['content'] = content
    json_data = json.dumps(payload)

    result = mqttClient.publish(mqtt_topic, json_data)           # Report data to MQTT server     
    status = result.rc
    if status != 0:
        print(f"Error al enviar mensaje a {mqtt_topic}: {status}")
    print(f"{datetime.now()} - {content}")


def noCache() -> str:
    # Generar un número aleatorio entre 0 y 1
    random_number : float = random.random()
    # Limitar la longitud a 18 dígitos en total (17 después del punto decimal)
    noCache : str = f"{random_number:.17f}"
    return noCache

def nextDay(now : date) -> date:
    # Por defecto dormimos hasta el día siguiente a las 00:01
    nextDay : date = now + timedelta(days=1)
    nextDay : datetime = datetime.combine(nextDay, datetime.min.time())
    nextDay = nextDay.replace(hour=0, minute=1)
    return nextDay

def equalsBooking(booking1 : Dict[str, Any], booking2 : Dict[str, Any]) -> bool:
    if (booking1["dateProgram"] == booking2["dateProgram"] and 
        booking1["dayOfWeek"] == booking2["dayOfWeek"] and
        booking1["timeStart"] == booking2["timeStart"] and
        booking1["activity"] == booking2["activity"]):
        return True
    else:
        return False

def findBooking(toFindBooking : Dict[str, Any], bookings : List[Dict[str, Any]]) -> Optional[Dict[str, Any]]:
    for booking in bookings:
        if (equalsBooking(toFindBooking, booking)):
            return booking
    return None

def getBookingEndDate(booking : Dict[str, Any]) -> date:
    bookingTimeStart : date = datetime.strptime(booking["timeEnd"], "%H:%M").time() 
    strBookingDateProgram :str = booking["dateProgram"] # Formato "2024-09-09"
    bookingDateProgram : date = date.fromisoformat(strBookingDateProgram)
    bookingDateProgram : date = datetime.combine(bookingDateProgram, bookingTimeStart)
    return bookingDateProgram

def getBookingStartDate(booking : Dict[str, Any]) -> date:
    bookingTimeStart : date = datetime.strptime(booking["timeStart"], "%H:%M").time() 
    strBookingDateProgram :str = booking["dateProgram"] # Formato "2024-09-09"
    bookingDateProgram : date = date.fromisoformat(strBookingDateProgram)
    bookingDateProgram : date = datetime.combine(bookingDateProgram, bookingTimeStart)
    return bookingDateProgram

def updateToSleepDatefromBookingDates(now : date, toSleepDate : date, bookings : List[Dict[str, Any]]) -> date:
    for booking in bookings:
        bookingEndDate : date = getBookingEndDate(booking)
        if (bookingEndDate < toSleepDate and bookingEndDate > now):
            # Validamos que la fecha de fin de la reserva sea menor que la fecha de dormir y mayor que la fecha actual            
            toSleepDate = bookingEndDate

    return toSleepDate

def calculateDiaSemana(now : date, dayOfWeek : str) -> date:
    # Leemos el día de la semana de la reserva
    bookingDayNumber : int = dias_semana_nombre_numero[dayOfWeek]
    # Obtener el día de la semana actual
    todayDayNumber : int = now.weekday()
    # Calculamos la distancia en días entre el día de la reserva y el día actual. Si es 6, es ayer, si es 0, es hoy.
    newBookingDaysDistance : int = (bookingDayNumber - todayDayNumber) % len(dias_semana_numero_nombre)

    if (newBookingDaysDistance == 0):
        # En caso que la distancia sea 0, significa que tenemos que avanzar una semana
        newBookingDaysDistance = len(dias_semana_numero_nombre)

    newBookingDate = now + timedelta(days=newBookingDaysDistance)
    return newBookingDate   

def resetLocalBookings(localBookings :List[Dict[str, Any]], now : date ) -> List[Dict[str, Any]]:
    for localBooking in localBookings:
        localBookingTimeEnd : date = datetime.strptime(localBooking["timeEnd"], "%H:%M").time()   
        if "dateProgram" in localBooking:
            localBookingDateProgram : date = date.fromisoformat(localBooking["dateProgram"])
        else:
            # Si no hay fecha de reserva, la calculamos a partir de la fecha actual y ponemos el dia de ayer por defecto
            localBookingDateProgram : date = now - timedelta(days=1)
        localBookingDateProgram = datetime.combine(localBookingDateProgram, localBookingTimeEnd)

        if "state" not in localBooking:
            # En caso que no esté informado el estado, se inicializa como no reservado
            localBooking["state"] = LocalBookingStates.NOTRESERVED.value

        # Si la reserva finaliza ahora miso o es del pasado, la marcamos como no reservada y actualizamos la fecha de la reserva
        if (now >= localBookingDateProgram):
            # Actualizamos el estado a no reservado para resetear la reserva.
            localBooking["state"] = LocalBookingStates.NOTRESERVED.value
            # Actualizamos la fecha de la reserva con la fecha futura el mismo día de la semana
            newBookingDate : date = calculateDiaSemana(now, localBooking["dayOfWeek"])
            localBooking["dateProgram"] = newBookingDate.strftime("%Y-%m-%d")
            print(f"Resetting local booking {localBooking}")                             

    return localBookings 

def login(configParserPrivate: configparser.ConfigParser)->Optional[requests.Session]:
    session : requests.Session
    username: str = configParserPrivate.get("login", "username") 

    sessionFile : str =  user + ".pk1"
    # Cargar la sesión desde un fichero
    try:
        with open(sessionFile, 'rb') as file:
            session = pickle.load(file)
        if getActivityGroups(session):
            print(f"Login with user {username} OK using session file {sessionFile}.")      
            return session            
    except Exception as e:        
        publish_mqtt(f"Error reading file {sessionFile}: {str(e)}")

    session = requests.Session()

    url : str = config_parser.get("login", "url")
    data : Dict[str, Any] = json.loads(config_parser.get("login", "data"))

    data["user"] = username
    data["pass"] = configParserPrivate.get("login", "password")

    response = session.post(url, data=data, headers=headers)

    # Verificamos la respuesta
    if response.status_code == 200:
        if getActivityGroups(session):        
            print(f"New Login with user {username} OK")      
            # Guardar la sesión en un fichero
            with open(sessionFile, 'wb') as file:            
                pickle.dump(session, file)
            return session
    
    publish_mqtt(f"Login with user {username} NOK: {response.status_code}")
    return None

def getActivityGroups(session : requests.Session) -> bool:
    url : str = config_parser.get("getActivityGroups", "url")

    # Sustituir los placeholders por los valores reales
    url = url.replace("{noCache}", noCache())

    response : requests.Response = session.get(url, headers=headers)
        
    # Verificamos la respuesta
    if response.status_code != 200:
        publish_mqtt(f"getActivityGroups NOK: {response.status_code}")
        return False
    
    return True

def getServerBookings(session : requests.Session) -> Optional[List[Dict[str, Any]]]:
    bookings : List[Dict[str, Any]] = []
    
    url : str = config_parser.get("myBookings", "url")

    # Sustituir los placeholders por los valores reales
    url = url.replace("{noCache}", noCache())

    response : requests.Response = session.get(url, headers=headers)
    
    # Verificamos la respuesta
    if response.status_code != 200:
        publish_mqtt(f"myBookings NOK:{response.status_code}")
        return None
    
    data : Dict[str, Any] = response.json()
    aaDatas : List[Dict[str, Any]] = data["aaData"]
    for aaData in aaDatas:
        strDateProgram :str = aaData["dateProgram"] # Formato "2024-09-09"
        dateProgram : date = date.fromisoformat(strDateProgram)
        if dateProgram >= date.today():
            # Nos guardamos los bookings futuros
            schedules : List[Dict[str, Any]] = aaData["schedules"]
            for schedule in schedules:
                # Miramos si la hora de la reserva es futura
                scheduleTimeStart : date = datetime.strptime(schedule["timeStart"], "%H:%M").time()   
                scheduleDateProgram : date = datetime.combine(dateProgram, scheduleTimeStart)
                if (scheduleDateProgram > datetime.now()):
                    booking : Dict[str, Any] = {}
                    booking["dateProgram"] = strDateProgram
                    booking["dayOfWeek"] = dias_semana_numero_nombre[dateProgram.weekday()]
                    booking["timeStart"] = schedule["timeStart"]
                    booking["timeEnd"] = schedule["timeEnd"]
                    booking["activity"] = schedule["activity"]["name"]
                    bookings.append(booking)

    return bookings

def getNumPreviousReservedLocalBookings(toFindlocalBooking : Dict[str, Any], localBookings : List[Dict[str, Any]]) -> int:
    numPreviousReservedLocalBookings : int = 0
    toFindlocalBookingDateProgram : date = getBookingStartDate(toFindlocalBooking)

    for localBooking in localBookings:
        if (localBooking["state"] == LocalBookingStates.RESERVED.value):
            numPreviousReservedLocalBookings += 1
        elif (localBooking["state"] == LocalBookingStates.INLIST.value):            
            if not equalsBooking(localBooking, toFindlocalBooking):
                localBookingDateProgram : date = getBookingStartDate(localBooking)
                if (localBookingDateProgram < toFindlocalBookingDateProgram):
                    # En caso que sea una reserva anterior que está en lista de espera, la contamos
                    numPreviousReservedLocalBookings += 1

    return numPreviousReservedLocalBookings

def getSchedules(session : requests.Session, strDateTime : str) -> Optional[List[Dict[str, Any]]]:
    mySchedules : List[Dict[str, Any]] = []

    url : str = config_parser.get("getSchedulesApp", "url")

    # Sustituir los placeholders por los valores reales
    url = url.replace("{startDateTime}", strDateTime)
    url = url.replace("{endtDateTime}", strDateTime)
    url = url.replace("{noCache}", noCache())
 
    response : requests.Response = session.get(url, headers=headers)    

    # Verificamos la respuesta
    if response.status_code != 200:
        publish_mqtt(f"getSchedulesApp NOK: {response.status_code}")
        return None
    
    schedules : Dict[str, Any] = response.json()
   
    if "calendar" in schedules:
        schedules : List[Dict[str, Any]] = schedules["calendar"]
        if len(schedules) == 1:
            schedules : Dict[str, Any] = schedules[0]
            if (schedules["dateProgram"] == strDateTime):
                dateProgram : date = date.fromisoformat(schedules["dateProgram"])
                for schedule in schedules["schedules"]:
                    mySchedule : Dict[str, Any] = {}
                    mySchedule["dateProgram"] = schedules["dateProgram"]
                    mySchedule["dayOfWeek"] = dias_semana_numero_nombre[dateProgram.weekday()]
                    mySchedule["timeStart"] = schedule["timeStart"]
                    mySchedule["timeEnd"] = schedule["timeEnd"]
                    mySchedule["activity"] = schedule["activity"]["name"]
                    mySchedule["bookingState"] = bookingStates[schedule["bookingState"]]
                    mySchedule["remainingPlaces"] = schedule["capacityAssistant"] - schedule["bookingInfo"]["bookedPlaces"]
                    mySchedules.append(mySchedule)

    return mySchedules

def saveConfigParserPrivate(bookings):
    config_parser_private.set("login", "bookings", json.dumps(bookings))    
        # Guardar los cambios en el archivo de configuración
    with open(config_ini_private, 'w') as configfile:            
        config_parser_private.write(configfile)

# Combinar las reservas locales con las del servidor
def mergeBookings(localBookings : List[Dict[str, Any]], serverBookings: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    for localBooking in localBookings:
        if (findBooking(localBooking, serverBookings) is not None):
            # Si encontramos la reserva en el servidor:
            if (localBooking["state"] == LocalBookingStates.NOTRESERVED.value or 
                localBooking["state"] == LocalBookingStates.CANCELLED.value or 
                localBooking["state"] == LocalBookingStates.INLIST.value):
                # Si la encontramos, actualizamos a reservada. Se trata del caso que reservemos en la app directamente o hemos entrado en lista.
                localBooking["state"] = LocalBookingStates.RESERVED.value
                print(f"Updated Local Booking to RESERVED: {localBooking}")
        else:
            # Si NO encontramos la reserva en el servidor:
            if (localBooking["state"] == LocalBookingStates.RESERVED.value):
                # Si la he cancelado en la app, no quiero que se vuelva a tramitar la reserva en local. La paso a un estado cancelado
                localBooking["state"] = LocalBookingStates.CANCELLED.value
                print(f"Updated Local Booking to CANCELLED: {localBooking}")
#            elif (localBooking["state"] == "NOTRESERVED" or 
#                localBooking["state"] == "CANCELLED" or 
#                localBooking["state"] == "INLIST"):
                # Es normal no encontrar la reserva en el servidor si no está reservada, está cancelada o está en lista de espera                

    return localBookings

def tratarBooking(localBooking : Dict[str, Any], localBookings : List[Dict[str, Any]], serverBookings: List[Dict[str, Any]], schedule: Dict[str, Any]) -> List[Dict[str, Any]]: 
    actions : List[Dict[str, Any]] = []

    if ( len(serverBookings) >= 2 or getNumPreviousReservedLocalBookings(localBooking, localBookings) >= 2 ):
        # Validamos si podemos hacer reservas. Hay que tener en cuenta las reservas locales que controlan si estamos en lista de espera
        # Si hay reservas previas en espera, no podemos reservar porque si no no entraremos en la lista de espera       

        if (schedule["bookingState"] == LocalBookingStates.RESERVED.value):
            # En caso que haya plazas disponibles, monitorizamos cuantas quedan       
            action : Dict[str, Any] = {}
            action["action"] = "MONITORICE"
            action["remainingPlaces"] = schedule["remainingPlaces"]
            actions.append(action)            
        elif (schedule["bookingState"] == LocalBookingStates.COMPLETED.value):
            action : Dict[str, Any] = {}
            action["action"] = "INLIST"
            actions.append(action)            
    else:
        if (schedule["bookingState"] == LocalBookingStates.RESERVED.value):
            action : Dict[str, Any] = {}
            action["action"] = "RESERVE"
            actions.append(action)            
        elif (schedule["bookingState"] == LocalBookingStates.COMPLETED.value):
            action : Dict[str, Any] = {}
            action["action"] = "INLIST"
            actions.append(action)            

    return actions


# Arrives (True) or Leave (False)
def report_mqtt(arrives, who, num_people_at_home):
    payload = {}
    
    payload['time'] = time.time_ns()                    # InfluxDB use nanoseconds for time field: https://discourse.nodered.org/t/time-stamp-and-influx-db-problem/13715
    payload['arrives'] = arrives
    payload['who'] = who
    payload['num_people_at_home'] = num_people_at_home
    json_data = json.dumps(payload)

    mqttClient.publish(mqtt_topic, json_data)           # Report data to MQTT server     

def doActions(actions : List[Dict[str, Any]], localBooking : Dict[str, Any], currentSleepDate : date, now : date) -> date:    
    toSleepDate : date = currentSleepDate

    for action in actions:
        if (action["action"] == "MONITORICE"):
            remainingPlaces : int = action["remainingPlaces"]
            actionToSleepDate = now + timedelta(minutes=plazas_disponibles_minutos_espera(remainingPlaces))
            if (actionToSleepDate < toSleepDate):
                toSleepDate = actionToSleepDate
            publish_mqtt(f"Monitorización plazas: {remainingPlaces} : {localBooking}")                

        elif (action["action"] == "RESERVE"):
            #En caso que podamos reservar, hacemos la reserva
            localBooking["state"] = LocalBookingStates.RESERVED.value
            publish_mqtt(f"Reservar : {localBooking}")                
        elif (action["action"] == "INLIST"):
            localBooking["state"] = LocalBookingStates.INLIST.value
            # Envia aviso para apuntarse a la lista de espera
            publish_mqtt(f"Ponerse en Lista de espera: {localBooking}")

    return toSleepDate

def bookingIteration() -> date:
    now : date = datetime.now()

    # Por defecto dormimos hasta el día siguiente a las 00:01
    toSleepDate : date = nextDay(now)

    localBookings: List[Dict[str, Any]] = json.loads(config_parser_private.get("login", "bookings"))
    
    localBookings = resetLocalBookings(localBookings, now)
    print(f"Local Bookings: {localBookings}")
    
    #Crear una sesión
    session : Optional[requests.Session] = login(config_parser_private)
    if session is None:
        return toSleepDate

    serverBookings : Optional[List[Dict[str, Any]]] = getServerBookings(session)
    print(f"Server Bookings: {serverBookings}")
    if serverBookings is None:
        return toSleepDate

    localBookings = mergeBookings(localBookings, serverBookings)
    toSleepDate = updateToSleepDatefromBookingDates(now, toSleepDate, localBookings)

    for localBooking in localBookings:
        # Si la reserva no está reservada, continuamos
        if (localBooking["state"] != LocalBookingStates.NOTRESERVED.value):
            break

        # Obtenemos las actividades del día de la reserva
        schedules : Optional[List[Dict[str, Any]]] = getSchedules(session, localBooking["dateProgram"])
        if schedules is None:
            break
            
        # Buscamos la reserva en las actividades del día
        schedule : Optional[Dict[str, Any]] = findBooking(schedules, localBooking)
        print(f"Found schedule: {schedule}")
        if schedule is None:
            break        

        actions : List[Dict[str, Any]] = tratarBooking(localBooking, localBookings, serverBookings, schedule)

        toSleepDate = doActions(actions, localBooking, toSleepDate)

    saveConfigParserPrivate(localBookings)

    return toSleepDate

#Inicio

if __name__ == "__main__":
    while True:        
        mqttClient : mqtt.Client = initMQTT()                                 # init MQTT client with config parameters
        if mqttClient is not None:
            break
        time.sleep(300)                                            # Wait 1 minute before trying to connect to MQTT server

    try:     
        publish_mqtt("Loading configuration OK")

        while True:
            toSleepDate : date = bookingIteration()
            publish_mqtt(f"Sleeping until {toSleepDate}")  
            time.sleep((toSleepDate - datetime.now()).total_seconds())
            
    except Exception as e:
        publish_mqtt(f"Error: {str(e)}")
        