#!/usr/bin/env python3

from datetime import date, datetime
from typing import Any, Dict, List
import pytest
from trainingymapp import calculateDiaSemana, equalsBooking, findBooking, getNumPreviousReservedLocalBookings, mergeBookings, resetLocalBookings, updateToSleepDatefromBookingDates  # Importar las funciones a probar

def test_equalsBooking_same():
    booking1 : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    booking2 : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    assert equalsBooking(booking1, booking2)

def test_equalsBooking_different_day():
    booking1 : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    booking2 : Dict[str, Any] = {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    assert not equalsBooking(booking1, booking2)

def test_equalsBooking_different_timeStart():
    booking1 : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    booking2 : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "23:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    assert not equalsBooking(booking1, booking2)

def test_findBooking_finded():
    booking : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    bookings : Dict[str, Any] = [{"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}, 
    {"dayOfWeek": "MARTES", "timeStart": "23:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}]

    result = findBooking(booking, bookings)

     # Aserción para verificar que el resultado no es None
    assert result is not None

def test_findBooking_notFind():
    booking : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}
    bookings : List[Dict[str, Any]] = [{"dayOfWeek": "MIERCOLES", "timeStart": "23:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}, 
    {"dayOfWeek": "MARTES", "timeStart": "23:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}]

    result = findBooking(booking, bookings)

     # Aserción para verificar que el resultado no es None
    assert result is None

def test_updateToSleepDatefromBookingDates_actualiza():
    bookings : List[Dict[str, Any]] = [{"dayOfWeek": "MIERCOLES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}, 
    {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-17"}]

    #Inicilizar una fecha a 2024-09-16 19:30
    dateTest = "2024-09-17"
    timeTest = "00:01"
    
    timeTest : date = datetime.strptime(timeTest, "%H:%M").time() 
    dateTest : date = date.fromisoformat(dateTest)
    dateTest : date = datetime.combine(dateTest, timeTest)

    toSleepDate : date = updateToSleepDatefromBookingDates(dateTest, bookings)

    assert toSleepDate == datetime.fromisoformat("2024-09-16 20:20")

def test_updateToSleepDatefromBookingDates_Noactualiza():
    bookings : List[Dict[str, Any]] = [{"dayOfWeek": "MIERCOLES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-17"}, 
    {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-18"}]

    #Inicilizar una fecha a 2024-09-16 19:30
    dateTest = "2024-09-17"
    timeTest = "00:01"    
    timeTest : date = datetime.strptime(timeTest, "%H:%M").time() 
    dateTest : date = date.fromisoformat(dateTest)
    dateTest : date = datetime.combine(dateTest, timeTest)

    toSleepDate : date = updateToSleepDatefromBookingDates(dateTest, bookings)

    assert toSleepDate == datetime.fromisoformat("2024-09-17 00:01")

def test_resetLocalBookings():
    bookings : List[Dict[str, Any]] = [
        {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-17"}, 
        {"dayOfWeek": "MIERCOLES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-18"},
        {"dayOfWeek": "JUEVES", "timeStart": "00:01", "timeEnd": "00:02", "activity": "BODY PUMP", "dateProgram": "2024-09-19"}]

    #Inicilizar una fecha a MIERCOLES
    dateTest = "2024-09-19"
    timeTest = "00:03"    
    timeTest : date = datetime.strptime(timeTest, "%H:%M").time() 
    dateTest : date = date.fromisoformat(dateTest)
    dateTest : date = datetime.combine(dateTest, timeTest)
    
    bookings = resetLocalBookings(bookings, dateTest)
    # Validamos que se avanzó una semana hasta el siguiente martes
    assert bookings[0]["dateProgram"] == "2024-09-24"
    assert bookings[1]["dateProgram"] == "2024-09-25"
    assert bookings[2]["dateProgram"] == "2024-09-26"

def test_calculateDiaSemana_same_day():
    now = date(2024, 9, 16)  # LUNES
    dayOfWeek = "LUNES"
    result = calculateDiaSemana(now, dayOfWeek)
    assert result == now

def test_calculateDiaSemana_next_day():
    now = date(2024, 9, 16)  # LUNES
    dayOfWeek = "MARTES"
    result = calculateDiaSemana(now, dayOfWeek)
    assert result == date(2024, 9, 17)

def test_calculateDiaSemana_previous_day():
    now = date(2024, 9, 16)  # LUNES
    dayOfWeek = "DOMINGO"
    result = calculateDiaSemana(now, dayOfWeek)
    assert result == date(2024, 9, 22)

def test_calculateDiaSemana_same_week():
    now = date(2024, 9, 16)  # LUNES
    dayOfWeek = "VIERNES"
    result = calculateDiaSemana(now, dayOfWeek)
    assert result == date(2024, 9, 20)

def test_calculateDiaSemana_next_week():
    now = date(2024, 9, 16)  # LUNES
    dayOfWeek = "LUNES"
    result = calculateDiaSemana(now, dayOfWeek)
    assert result == date(2024, 9, 23)

def test_mergeBookings():
    localBookings : List[Dict[str, Any]] = [
        {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "NOTRESERVED", "dateProgram": "2024-09-16"}, 
        {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "CANCELLED", "dateProgram": "2024-09-17"}, 
        {"dayOfWeek": "MIERCOLES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-18"},
        {"dayOfWeek": "JUEVES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "RESERVED", "dateProgram": "2024-09-19"},
        {"dayOfWeek": "VIERNES", "timeStart": "19:30", "timeEnd": "20:00", "activity": "BODY PUMP", "state": "RESERVED", "dateProgram": "2024-09-20"}]
    serverBookings: List[Dict[str, Any]] = [
        {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-16"}, 
        {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-17"}, 
        {"dayOfWeek": "MIERCOLES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-18"},
        {"dayOfWeek": "JUEVES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "dateProgram": "2024-09-19"}]

    localBookings = mergeBookings(localBookings, serverBookings)
    # Reserva en servidor de reserva local NOTRESERVED
    assert localBookings[0]["state"] == "RESERVED"
    # Reserva en servidor de reserva local CANCELADA, se vuelve a RESERVAR
    assert localBookings[1]["state"] == "RESERVED"
    # Reserva en servidor de reserva local INLIST, se pasa a RESERVADA. 
    assert localBookings[2]["state"] == "RESERVED"
    # Reserva en servidor de reserva local RESERVED, se queda RESERVADA.
    assert localBookings[3]["state"] == "RESERVED"
    # Reserva en servidor de reserva local RESERVED que no se encuentra ahora en el servidor. Se cancela.
    assert localBookings[4]["state"] == "CANCELLED"

def test_getNumPreviousReservedLocalBookings():
    localBookings : List[Dict[str, Any]] = [
        {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-16"}, 
        {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-17"}, 
        {"dayOfWeek": "MIERCOLES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "RESERVED", "dateProgram": "2024-09-18"},
        {"dayOfWeek": "JUEVES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-19"},
        {"dayOfWeek": "VIERNES", "timeStart": "19:30", "timeEnd": "20:00", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-20"}]
    
    toFindlocalBooking : Dict[str, Any] = {"dayOfWeek": "LUNES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-16"}
    # Contabiliza uno porque hay una reserva
    assert getNumPreviousReservedLocalBookings(toFindlocalBooking, localBookings) == 1

    toFindlocalBooking : Dict[str, Any] = {"dayOfWeek": "MARTES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-17"}
    # Contabiliza uno porque hay una reserva y otra por la del Lunes que está en Lista.
    assert getNumPreviousReservedLocalBookings(toFindlocalBooking, localBookings) == 2

    toFindlocalBooking : Dict[str, Any] = {"dayOfWeek": "JUEVES", "timeStart": "19:30", "timeEnd": "20:20", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-19"}
    # Contabiliza uno porque hay una reserva y otra por la del Lunes que está en Lista.
    assert getNumPreviousReservedLocalBookings(toFindlocalBooking, localBookings) == 3

    toFindlocalBooking : Dict[str, Any] = {"dayOfWeek": "VIERNES", "timeStart": "19:30", "timeEnd": "20:00", "activity": "BODY PUMP", "state": "INLIST", "dateProgram": "2024-09-20"}
    # Contabiliza uno porque hay una reserva y otra por la del Lunes que está en Lista.
    assert getNumPreviousReservedLocalBookings(toFindlocalBooking, localBookings) == 4

