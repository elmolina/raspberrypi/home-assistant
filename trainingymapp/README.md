# Home Assistant

## TrainingGymApp Monitori v1
- Configure trainingymapp.ini. Set user as your username in trainingymapp.com
- Create de user configuration file named : "{user}_private.ini" with this content:
    [login]
    username = {username}
    password = {password}
    bookings = [{"dayOfWeek": "XXXX", "timeStart": "XX:XX", "timeEnd": "XX:XX", "activity": "XXXXX"}, {"dayOfWeek": "XXXX", "timeStart": "XX:XX", "timeEnd": "XX:XX", "activity": "XXXX"}]
- Copy trainingymapp.sh file to /etc/init.d, and make it executable:
    ```
    sudo cp trainingymapp.sh /etc/init.d/
    sudo chmod +x /etc/init.d/trainingymapp.sh
    sudo touch /var/log/trainingymapp.log && sudo chown elmol /var/log/trainingymapp.log
    sudo touch /var/run/trainingymapp.pid && sudo chown elmol /var/run/trainingymapp.pid
    ```
  - Now we'll register the script to be run on boot:
    ```
    sudo update-rc.d trainingymapp.sh defaults
    ```
  - Now, you can either restart your machine, or kick this off manually since it won't already be running:
    ```
    sudo reboot
    ```
    or
    ```
    sudo /etc/init.d/trainingymapp.sh start
    ```
- Other commands
    ```
    sudo /etc/init.d/trainingymapp.sh stop
    sudo /etc/init.d/trainingymapp.sh restart
    ```