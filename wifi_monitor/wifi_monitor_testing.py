#!/usr/bin/env python3

import subprocess
import configparser
import paho.mqtt.client as mqtt                        # see documentation in https://github.com/eclipse/paho.mqtt.python
import json
import time
import numpy as np
from pathlib import Path

last_who_is_at_home = []
who_is_at_home = ['Tere', 'Raúl']

if not np.array_equal(who_is_at_home, last_who_is_at_home): 
    result = list(set(last_who_is_at_home) - set(who_is_at_home))
    print ("Leaves :" + str(result))
    
    result = list(set(who_is_at_home) - set(last_who_is_at_home))
    print ("Arrives :" + str(result))