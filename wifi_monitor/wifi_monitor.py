#!/usr/bin/env python3

import subprocess
import configparser
import paho.mqtt.client as mqtt                        # see documentation in https://github.com/eclipse/paho.mqtt.python
import json
import time
import numpy as np
from pathlib import Path

config_ini = "wifi_monitor.ini"
config_parser = configparser.ConfigParser()
config_parser.optionxform = str
config_parser.read(config_ini)                         # read config file and store its values in config_parser variable 

sampletime = config_parser.getint("Wifi", "sampletime")
subnet = config_parser.get("Wifi", "subnet")
retain_steps = config_parser.getint("Wifi", "retain_steps")
mqtt_topic = config_parser.get("MQTT", "topic")
owners_ips_filename =  config_parser.get("Data", "owners_ips_filename")

config_ini_private = "wifi_monitor_private.ini"
                                                        # [Devices]
                                                        # macs: [XX:XX:XX:XX:XX:XX, YY:YY:YY:YY:YY:YY]
                                                        # owners: [Person1, Person2]
config_parser_private = configparser.ConfigParser()
config_parser_private.optionxform = str
config_parser_private.read(config_ini_private)          # read config file and store its values in config_parser variable 

owners_macs = config_parser_private.items("macs")

def initMQTT():
    mqttClient = mqtt.Client()
    try:         
                                                         # Starts a new thread, that calls the loop method at regular intervals for you. It also handles re-connects automatically.
        mqttClient.loop_start()                          # Non blocking method. We want to manage the fan though no MQTT server is present
        mqtt_hostname = config_parser.get("MQTT", "host")    
        mqttClient.connect(mqtt_hostname)
        return mqttClient
    except:                                                     # Error control. The program will continue though there is no MQTT server
        return mqttClient

def save_dict(dict, filename):
    with open(filename, 'w') as f:
        f.write(json.dumps(dict))

def load_dict(filename):
    dict = {}
    file = Path(filename)
    if file.is_file():                          # file exists
        with open(filename) as f:
            dict = json.loads(f.read())
    return dict

def fping(subnet):
    cmd_fping =   "fping -q -g " + subnet +"-c 3 2>/dev/null"
    
    process_fping = subprocess.Popen(cmd_fping, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out_process_fping, err_process_fping = process_fping.communicate()

def ping(owners_ips):
    for owner_ip in owners_ips.values():
        cmd_ping =   "ping -c 1 " + owner_ip + " -q 1>/dev/null"
        process_ping = subprocess.Popen(cmd_ping, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out_process_fping, err_process_fping = process_ping.communicate()

def arp(owners_macs):
    grep_macs = ""
    for owner, owner_mac in owners_macs:
        grep_macs = grep_macs + owner_mac + '|'
    grep_macs = grep_macs[:-1]                                  #remove last character |
    
    cmd_ips_macs_at_home = "arp -e | grep -E '" + grep_macs + "' | awk '{ print $1, $3 }'"    
    
    process_ips_macs_at_home = subprocess.Popen(cmd_ips_macs_at_home, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out_process_ips_macs_at_home, err_process_ips_macs_at_home = process_ips_macs_at_home.communicate()

    ips_macs_at_home = out_process_ips_macs_at_home.decode()    # byte to string
    ips_macs_at_home = ips_macs_at_home.split("\n")             # list all lines
    ips_macs_at_home = ips_macs_at_home[:-1]                    # remove last entry because is always empty with carry return
    return ips_macs_at_home
   
# Arrives (True) or Leave (False)
def report_mqtt(arrives, who, num_people_at_home):
    payload = {}
    
    payload['time'] = time.time_ns()                    # InfluxDB use nanoseconds for time field: https://discourse.nodered.org/t/time-stamp-and-influx-db-problem/13715
    payload['arrives'] = arrives
    payload['who'] = who
    payload['num_people_at_home'] = num_people_at_home
    json_data = json.dumps(payload)

    if (not arrives):
        retained_messages[who] = {"json": json_data, "count": 0}
    else:         
        pop_key = retained_messages.pop(who, None) 
        if (pop_key == None):
            mqttClient.publish(mqtt_topic, json_data)           # Report data to MQTT server      

def process_retained_messages():
    for retain_message_who in list(retained_messages):
        retain_message = retained_messages[retain_message_who]
        if retain_message["count"] == retain_steps:
            mqttClient.publish(mqtt_topic, retain_message["json"])
            retained_messages.pop(retain_message_who)
        else:
            retain_message["count"] += 1

owners_ips = load_dict(owners_ips_filename)             # table of IP's assigned to every owner device 

mqttClient = initMQTT()                                 # init MQTT client with config parameters

print ("Loading configuration OK")

last_who_is_at_home = []                                # List of people at home in the last execution
retained_messages = {}

while True:
    #if (len(owners_ips) != len(owners_macs)):
    #    fping(subnet)

    ping(owners_ips)                                    # Perform a ping to every phone 
    
    ips_macs_at_home = arp(owners_macs)                 # Get Macs of the connected phones.

    owners_ips_changed = False

    who_is_at_home = []
    for ip_mac_at_home in ips_macs_at_home:
        tuple_ip_mac_at_home = ip_mac_at_home.split()
        ip_at_home = tuple_ip_mac_at_home[0]
        mac_at_home = tuple_ip_mac_at_home[1]        
        for owner, owner_mac in owners_macs:
            if mac_at_home == owner_mac:
                who_is_at_home.append(owner)
                if owners_ips.get(owner) != ip_at_home:
                    owners_ips[owner] = ip_at_home
                    owners_ips_changed = True

    print(json.dumps(who_is_at_home))

    if owners_ips_changed:
        save_dict(owners_ips, owners_ips_filename)

    if not np.array_equal(who_is_at_home, last_who_is_at_home):                     
        who_leaves = list(set(last_who_is_at_home) - set(who_is_at_home))            
        who_arrives = list(set(who_is_at_home) - set(last_who_is_at_home))          

        for who_leave in who_leaves:
            report_mqtt(False, who_leave, len(who_is_at_home))               # Create json with data            

        for who_arrive in who_arrives:
            report_mqtt(True, who_arrive, len(who_is_at_home))               # Create json with data            

        last_who_is_at_home = who_is_at_home
    
    process_retained_messages()
    time.sleep(sampletime)                                                          