# Home Assistant

## Get IOTstack
## Deploy Home Assistant docker image
- Follow https://sensorsiot.github.io/IOTstack/Containers/Home-Assistant/ (Installing Home Assistant Container) 
## Install HACS
- Follow https://peyanski.com/how-to-install-home-assistant-community-store-hacs/ (Initial install of Home Assistant Community Store (HACS) on Home Assistant Container)
- Enter to home assistant http://<host>:8123/
## Install Meross Cloud IoT
- Follow https://github.com/albertogeniola/meross-homeassistant1
## Alternative: Install Meros Lan (Best)
- https://github.com/krahabb/meross_lan
## Samsung Tv integration
- Requires TV config "Samsung menu is settings- general-external device manager- device connection manager- First settings (access notification) set to first time only"
## Monitoring wifi v1
- sudo apt install fping
- fping -q -g 192.168.1.0/24 -c 3 2>/dev/null; arp -e | grep -E '60:6e:e8:2a:25:84|bc:7f:a4:27:3a:21' | awk '{ print $1, $3 }'
- Sometimes does not recognise new devices. Is needed an explicit
- ping -c 1 192.168.1.133 -q 1>/dev/null; ping -c 1 192.168.1.134 1>/dev/null; arp -e | grep -E '60:6e:e8:2a:25:84|bc:7f:a4:27:3a:21' | awk '{ print $1, $3 }'
- arp-scan

## Install instructions
 Make fancontrol.sh executable on boot:
  - Configure wifi_monitor.ini and wifi_monitor_private.ini
  - Copy wifi_monitor.sh file to /etc/init.d, and make it executable:
    ```
    sudo cp wifi_monitor.sh /etc/init.d/
    sudo chmod +x /etc/init.d/wifi_monitor.sh
    sudo touch /var/log/wifi_monitor.log && sudo chown elmol /var/log/wifi_monitor.log
    
    ```
  - Now we'll register the script to be run on boot:
    ```
    sudo update-rc.d wifi_monitor.sh defaults
    ```
  - Now, you can either restart your machine, or kick this off manually since it won't already be running:
    ```
    sudo reboot
    ```
    or
    ```
    sudo /etc/init.d/wifi_monitor.sh start
    ```